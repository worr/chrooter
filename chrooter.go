package chrooter

import (
	"strconv"
	"syscall"
	"os"
	"os/user"
	"path/filepath"
)

func Chroot(username string, directory string) error {
	var luser *user.User
	var err error
	var uid int
	var gid int

	if luser, err = user.Lookup(username); err != nil {
		return err
	}

	if uid, err = strconv.Atoi(luser.Uid); err != nil {
		return err
	}

	if gid, err = strconv.Atoi(luser.Gid); err != nil {
		return err
	}

	if _, err = os.Stat(directory); os.IsNotExist(err) {
		buildResolver(directory)
		if err = filepath.Walk(directory, func(path string, info os.FileInfo, err error) error { return os.Chown(path, uid, gid) }); err != nil {
			return err
		}
	}

	if err = os.Chdir(directory); err != nil {
		return err
	}

	if err = syscall.Chroot(directory); err != nil {
		return err
	}

	if err = os.Chdir("/"); err != nil {
		return err
	}

	if err = syscall.Setregid(gid, gid); err != nil {
		return err
	}

	if err = syscall.Setreuid(uid, uid); err != nil {
		return err
	}

	return nil
}
