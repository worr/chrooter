package chrooter

import (
	"fmt"
	"io"
	"os"
	"path"
)

func copyFile(source string, dest string) error {
	var err error

	// Ensure that source exists
	var sourcefi os.FileInfo
	if sourcefi, err = os.Stat(source); os.IsNotExist(err) {
		return fmt.Errorf("%v does not exist", source)
	}

	// Ensure that dest directory exists
	var destfi os.FileInfo
	if destfi, err = os.Stat(dest); os.IsNotExist(err) {
		return fmt.Errorf("%v does not exist", dest)
	}

	// Ensure that dest is a directory
	if !destfi.IsDir() {
		return fmt.Errorf("%v is not a directory", dest)
	}

	// Check if full path exists
	fullDest := path.Join(dest, path.Base(source))
	if destfi, err = os.Stat(fullDest); os.IsExist(err) {
		return fmt.Errorf("%v exists", fullDest)
	}

	// Ensure that dest is not directory
	if sourcefi.IsDir() {
		return fmt.Errorf("%v is a directory")
	}

	var destFile *os.File
	var sourceFile *os.File
	if sourceFile, err = os.Open(source); err != nil {
		return err
	}

	defer sourceFile.Close()

	if destFile, err = os.Create(dest); err != nil {
		return err
	}

	if _, err = io.Copy(destFile, sourceFile); err != nil {
		destFile.Close()
		return err
	}

	return destFile.Close()
}

func buildResolver(dir string) error {
	resolverFiles := make([]string, 2)
	resolverFiles = append(resolverFiles, "/etc/resolv.conf", "/etc/hosts")

	if err := buildBasic(dir); err != nil {
		return err
	}

	var err error
	var etcfi os.FileInfo
	if etcfi, err = os.Stat("/etc"); err != nil {
		return err
	}

	basePath := path.Join(dir, "etc")
	if err := os.Mkdir(basePath, etcfi.Mode()); err != nil {
		return err
	}

	for _, file := range resolverFiles {
		if err = copyFile(file, path.Join(dir, path.Base(file))); err != nil {
			return err
		}
	}

	return nil
}

func buildBasic(dir string) error {
	if _, err := os.Stat(dir); os.IsNotExist(err) {
		if err = os.Mkdir(dir, 0755); err != nil {
			return err
		}
	}

	return nil
}
